# How to Set Up Kubernetes Monitoring and Logging on DigitalOcean with Prometheus, Loki and Grafana

A DigitalOcean article on setting up monitoring and logging in a DigitalOcean Kubernetes cluster.

## Requirements
The documentation is written in [Asciidoctor](https://asciidoctor.org) syntax. On OS X
we recommend installing it with Homebrew: `brew install asciidoctor`.

## Building
To build the documentation in HTML format, just invoke asciidoctor on the main document:
```
$ asciidoctor index.adoc
```
