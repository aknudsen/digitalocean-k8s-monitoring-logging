= How to Set Up Monitoring and Logging on DigitalOcean Kubernetes with Prometheus, Loki and Grafana
Arve Knudsen <arve.knudsen@gmail.com>
:toc: left
:article:

[abstract]
== Introduction
Tracking container logs is an essential part of a Kubernetes cluster, and support for this
is built into the Kubernetes platform itself. However, the built-in support is highly primitive,
e.g. you can print the logs of containers via the `kubectl` command line tool and in a production
cluster you will quickly want for a more advanced setup, typically with a database of logs that
you can search from a graphical user interface.

The traditionally popular logging combo to combine with Kubernetes is the 
https://www.elastic.co/products/elasticsearch[Elasticsearch^] backend, with its dedicated
front-end https://www.elastic.co/products/kibana[Kibana^]. Elasticsearch is a tricky/demanding
system to administrate yourself though, and most professionals in the field recommend using a
managed service instead.

There's a new kid on the block however to challenge Elasticsearch's hegemony,
https://grafana.com/loki[Loki^]. This is a lighter and simpler log database, that's designed
from the ground up to be _cloud native_ and easy to operate in Kubernetes clusters. Furthermore,
it has https://grafana.com/grafana[Grafana^] as its designated graphical interface, which means
that you can use the latter for both visualizing monitoring metrics _and_ browsing logs
(as opposed to using both Grafana and Kibana). Loki is at the time of writing alpha level
software, and as such not a mature/stable product yet, but we believe it's worth trying already
now on account of its (promised) administrative ease/efficiency and integration with Grafana
(removing the need to install Kibana).

On the subject of Kubernetes monitoring, the current popular solution in this regard is
the https://prometheus.io/[Prometheus^]/link:https://github.com/prometheus/alertmanager[Alertmanager^]
stack coupled with Grafana as the front-end for visualizing metrics. Prometheus itself is a time
series database and monitoring tool that works by collecting metrics from special container 
endpoints. You can query this data directly using the PromQL language, or you can visualize
it in Grafana (the most straightforward solution). Alertmanager is a supplementary service
that has the role of alerting on metrics from Prometheus, f.ex. when CPU usage reaches a certain 
threshold. The cluster operator will configure Alertmanager according to their needs, wrt. the 
kinds of alerts and where to send them (e.g. email/Slack).

It's a highly complicated task to deploy a fully configured stack consisting of Loki, Kubernetes, 
Alertmanager and Grafana to Kubernetes. In this article we will take you through the steps to
install each of them, with comprehensive configuration such as Grafana monitoring dashboards, to a
https://www.digitalocean.com/products/kubernetes/[DigitalOcean Kubernetes^] cluster with a
minimum of effort on the part of the reader.

We will be using a comprehensive https://jsonnet.org/[Jsonnet^] library, https://gitlab.com/aknuds1/kube-loki-prometheus[Kube-Loki-Prometheus^], in order to install the Prometheus/Alertmanager/
Loki/Grafana parts of the stack. It builds in turn on the excellent
https://github.com/coreos/kube-prometheus[Kube-Prometheus^] library, which we believe to be the
very best option to install and operate Prometheus in Kubernetes
as it makes use of the https://github.com/coreos/prometheus-operator[Prometheus Operator^] that 
intelligently creates and manages Prometheus instances in the cluster. It also installs Grafana
configured to integrate with Prometheus and bundles it with a solid library of Grafana monitoring 
dashboards. Loki gets deployed as a datasource to Grafana, so the latter exposes logs gathered
by Loki.

include::prerequisites.adoc[]

include::deploying-loki-and-prometheus.adoc[]

include::exposing-grafana.adoc[]

include::accessing-grafana.adoc[]

include::conclusion.adoc[]

