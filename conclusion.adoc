== Conclusion
In this tutorial you installed a Loki, Kube-Prometheus and Grafana stack into a
DigitalOcean Kubernetes cluster with a standard set of dashboards, monitoring rules and
alerts. Additionally, you configured an OAuth2 authenticating ingress for acessing your
Grafana UI from the Internet. Well done!

To re-cap, https://github.com/coreos/kube-prometheus[Kube-Prometheus^] is in fact
a bundle of https://github.com/coreos/prometheus-operator[Prometheus Operator^], a Kubernetes
https://coreos.com/operators/[operator^] that intelligently installs and manages Prometheus and
Alertmanager clusters within Kubernetes, Grafana, monitoring/alerting rules and Grafana dashboards.

In order to learn more about configuring the individual components of the stack, for example
to introduce custom Prometheus metrics and Grafana dashboards or to adjust alerting rules,
please consult their individual websites:

* https://grafana.com/loki[Loki^]
* https://github.com/coreos/kube-prometheus[Kube-Prometheus^]
* https://github.com/coreos/prometheus-operator[Prometheus Operator^]
* https://prometheus.io/[Prometheus^]
* https://prometheus.io/docs/alerting/alertmanager/[Alertmanager^]
* https://grafana.com/grafana[Grafana^]
